"""store stage"""
import sys
import pygame
from pygame.locals import QUIT, K_LCTRL, \
                          K_RCTRL, K_q, K_m, \
                          FULLSCREEN, RESIZABLE
from settings import Settings

def terminate():
    """Quit game"""
    pygame.quit()
    sys.exit()

class Game():
    """Game module"""
    def __init__(self):
        Settings().window_surface: pygame.Surface = \
            pygame.display.set_mode(size=(860, 540),
                                    # flags=RESIZABLE,
                                    display=0)

        Settings().window_width, Settings().window_height = Settings().window_surface.get_size()
        self.fps = 60
        pygame.display.set_caption("Simple platformer")

    def draw_level_1(self):
        """draw level 1"""
        from levels.level1 import Level1
        level = Level1()
        level.draw_ground()

    def event_handler(self):
        """Event handler"""
        pressed = pygame.key.get_pressed()
        if (pressed[K_LCTRL] or pressed[K_RCTRL]) and pressed[K_q]:
            terminate()
        for event in pygame.event.get():
            if event.type == QUIT:
                terminate()
