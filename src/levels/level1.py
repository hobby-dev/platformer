"""level1"""
import pygame
from settings import Settings

class Level1():
    """level 1"""
    def __init__(self):
        ground_image = pygame.image.load('data/ground.png')
        self.ground_sprite = pygame.transform.rotozoom(
            ground_image, 0, (Settings().window_width / 15) / ground_image.get_height())

    def draw_ground(self):
        """Render background"""
        ground_x = 0
        ground_y = Settings().window_height - self.ground_sprite.get_height()
        while ground_x < Settings().window_width:
            Settings().window_surface.blit(self.ground_sprite, (ground_x, ground_y))
            ground_x += self.ground_sprite.get_width()
